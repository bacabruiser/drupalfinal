<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
  
  

  
 
  

  
  
  
  
  
  
  
  
 
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Top Background Image Wrapper -->
<div class="bgded header-image"> 

  <!-- ################################################################################################ -->
  <!--<img src="/drupalFinal/app/<?php print path_to_theme(); ?>/images/demo/backgrounds/007.jpg" title="Background Image" alt="Background Image" />!-->

  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"> 
      <!-- ################################################################################################ -->
      
      <div class="fl_right">
        <ul class="faico clear">
          <li><a class="faicon-facebook" href="https://www.facebook.com/streeteatsdsm/?fref=ts"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-twitter" href="https://twitter.com/StreetEatsDSM"><i class="fa fa-twitter"></i></a></li>
         
        
        </ul>
      </div>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <div class="wrapper row1">
    <header id="header" class="hoc clear"> 
      <!-- ################################################################################################ -->
      
      <div id="logo" class="fl_left">
        <h1><a href="index.html">Street Eats DSM</a></h1>
      </div>
      <div id="quickinfo" class="fl_right">
        <ul class="nospace inline">
          <li><strong>Have A Chat:</strong><br>
            (515) 414 4742</li>
        </ul>
      </div>
      <!-- ################################################################################################ -->
    </header>
  </div>
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <div class="wrapper row2">
    <nav id="mainav" class="hoc clear"> 
      
    <!-- ################################################################################################ -->
     <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix')), 'heading' => t(''))); ?>
      
        
      <!-- ################################################################################################ -->
    </nav>
  </div>
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <div class="wrapper overlay">
    <div id="pageintro" class="hoc clear"> 
      <!-- ################################################################################################ -->
      <div class="introtxt">
          <h2 class="heading underline center">Voted by YOU!!</h2><br><h2 class="heading underline center"> The Best Sammiches in Des Moines!!</h2>
       
      </div>
      <!-- ################################################################################################ -->
     
     
      <!-- ################################################################################################ -->
      <div class="clear"></div>
    </div>
  </div>
  <!-- ################################################################################################ -->
</div>
<!-- End Top Background Image Wrapper -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<?php if ($is_front): ?>
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <ul class="nospace group categories">
        
         
       

    
      
      <li>
        <?php if ($page['featured']): ?><div id="featured"><?php print render($page['featured']); ?></div><?php endif; ?>
        
         
                  
      </li>
    </ul>
    </main>
</div> 
<?php endif; ?>  
      
   
    <!-- ################################################################################################ -->
    <!-- / main body -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<?php if (!$is_front): ?>
<div class="wrapper bgded overlay coloured">
<?php print render($page['content']); ?>  
</div>
<?php endif; ?>

<?php if ($is_front): ?>
<div class="wrapper bgded overlay coloured">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    
    <ul class="nospace group categories">
    
      <li>
       
          <?php if ($page['upcoming_events']): ?><div id="featured"><?php print render($page['upcoming_events']); ?></div>
       <?php endif; ?>
          
      </li>
      
    </ul>

    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </section>
</div>
<?php endif; ?>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<?php if ($is_front): ?>
<div class="wrapper row3">
  <section class="hoc container clear"> 
    <!-- ################################################################################################ -->
    
    
    
      
       <?php if ($page['about_us']): ?>
        <div id="about_us" class="center"><div class="section"><?php print render($page['about_us']); ?>
            <?php endif; ?>
            </div>
        </div>
        
    
<?php endif; ?>    
    
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </section>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row4 bgded overlay" style="background-image:url('images/demo/backgrounds/03.png');">
  <footer id="footer" class="hoc topspace-0 clear"> 
    <!-- ################################################################################################ -->
   
    <!-- ################################################################################################ -->
    <div class="group">
      <div class="one_quarter first">
       
          <?php print render($page['footer']); ?>
          
      </div>
     </div>
     
       
       
   
    
    <!-- ################################################################################################ -->
  </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->

<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->











    